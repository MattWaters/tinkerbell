from datetime import datetime
from typing import TYPE_CHECKING

from tinkerbell.integrations import ESClient, RedditClient

if TYPE_CHECKING:
    from tinkerbell.storage import Storage


class Tinkerbell(object):
    def __init__(self, cluster_host: str, storage_client: Storage, subreddit: str) -> None:
        # Passing a storage client instance allows the storage to be abstracted, so more storage plugins can be written.
        # As long as they implement the fetch_praw_config method to return a python dict of praw config arguments
        # and are child classes of tinkerbell.storage.Storage, they'll work.
        self.reddit = RedditClient(
            storage_client=storage_client,
            subreddit=subreddit
        )

        self.es = ESClient(cluster_host=cluster_host, index_name=subreddit)

        results = self.es.search("range", created={"lte": "now"})

        if results:
            self.last_post = results[0]["created"].split(" ")[0].replace("-", "/")
        else:
            # We give it a default value just in case the subreddit has never had any posts
            self.last_post = "1970/01/01"

    def run(self) -> None:
        print("Date of last post in index: {}").format(self.last_post)

        submissions = self.reddit.fetch_submissions(self.last_post, datetime.now().strftime("%Y/%m/%d"))

        self.es.index_submissions(submissions)

        print("Index updated.")

    def run_continuously(self) -> None:
        self.run()

        for submission in self.reddit.stream_submissions():
            self.es.index_submissions([submission])
