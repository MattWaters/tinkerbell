from datetime import datetime
from typing import Dict, List, TYPE_CHECKING

from elasticsearch import Elasticsearch, RequestsHttpConnection
from elasticsearch_dsl import Search

if TYPE_CHECKING:
    from elasticsearch_dsl.search import Response
    from praw.models import Submission


class ESClient(object):
    def __init__(self, cluster_host: str, index_name: str) -> None:
        self.cluster = Elasticsearch(
            host=cluster_host,
            transport_class=RequestsHttpConnection
        )

        self.es_search = Search(using=self.cluster, index=index_name)

        self.index_name = index_name

        if not self.cluster.indices.exists(self.index_name):
            self.cluster.indices.create(
                index=self.index_name,
                body={
                    "mappings": {
                        "post": {
                            "properties": {
                                "created": {
                                    "type": "date",
                                    "format": "yyyy-MM-dd HH:mm:ss"
                                },
                                "author": {
                                    "type": "nested",
                                    "properties": {
                                        "name": {"type": "keyword"},
                                        "created": {"type": "date", "format": "yyyy-MM-dd HH:mm:ss"},
                                    }},
                                "flair": {
                                    "type": "keyword"
                                },
                                "domain": {
                                    "type": "keyword"
                                }
                            }
                        }
                    }
                }
            )

    def index_submissions(self, submissions: List[Submission]) -> None:
        for submission in submissions:
            self.cluster.index(
                index=self.index_name,
                doc_type="post",
                id=submission.id,
                body={
                    "id": submission.id,
                    "url": submission.url,
                    "created": datetime.utcfromtimestamp(
                        int(submission.created_utc)).strftime('%Y-%m-%d %H:%M:%S'),
                    "title": submission.title,
                    "flair": submission.link_flair_text,
                    "views": submission.view_count,
                    "comment_count": submission.num_comments,
                    "submission_text": submission.selftext,
                    "domain": submission.domain,
                    "author": {
                        "author_name": str(submission.author),
                        "account_created": None,
                        "account_age": None,
                        "is_banned": None
                    },
                    "karma": submission.score,
                    "upvotes": submission.ups,
                    "downvotes": submission.downs
                }
            )

    def search(self, search_type: str, **filters: Dict[str, str]) -> List[Response]:
        return self.es_search.query(search_type, **filters).execute()
