import time
from datetime import datetime
from typing import List, TYPE_CHECKING

from praw import Reddit

if TYPE_CHECKING:
    from praw.models import Submission

    from tinkerbell.storage import Storage


class RedditClient(object):
    def __init__(self, storage_client: Storage, subreddit: str) -> None:
        praw_config = storage_client.fetch_praw_config()

        praw_config["sub"] = subreddit

        self.praw = Reddit(**praw_config)
        self.subreddit = self.praw.subreddit(subreddit)

        self.stream_submissions = self.subreddit.stream.submissions

    def fetch_submissions(self, start_date: str, end_date: str) -> List[Submission]:
        start_date = time.mktime(
            datetime.strptime(
                start_date, '%Y/%m/%d'
            ).timetuple()
        )

        end_date = time.mktime(
            datetime.strptime(
                end_date, '%Y/%m/%d'
            ).timetuple()
        )

        submissions = [item for item in self.subreddit.submissions(start_date, end_date)]

        return submissions
