from tinkerbell import Tinkerbell
from tinkerbell.storage import S3Client

t = Tinkerbell(
    cluster_host="172.18.0.1",
    storage_client=S3Client(
        aws_access_key=os.environ.get("AWS_ACCESS_KEY_ID", None),
        aws_secret_key=os.environ.get("AWS_SECRET_ACCESS_KEY", None),
        bucket_name="tinkerbell",
        object_key="praw.json"
    ),
    subreddit="anxiety"
)

t.run_continuously()
