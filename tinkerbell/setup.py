from setuptools import setup

setup(
    name="Tinkerbell",
    version="0.1",
    long_description=__doc__,
    packages=["tinkerbell"],
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        "boto3",
        "elasticsearch",
        "elasticsearch_dsl",
        "praw"
    ],
    extras_require={
        "dev": [
            "ipython",
            "pytest",
            "pytest-pep8",
            "pytest-cov"
        ]
    }
)
