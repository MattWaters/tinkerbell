from tinkerbell.storage.storage import Storage
from tinkerbell.storage.s3 import S3Client
from tinkerbell.storage.local_file import LocalFileClient
