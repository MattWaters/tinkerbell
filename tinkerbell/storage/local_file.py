import json
from typing import Dict

from tinkerbell.storage import Storage


class LocalFileClient(Storage):
    def __init__(self, file_path: str) -> None:
        self.file_path = file_path

    def fetch_praw_config(self) -> Dict[str, str]:
        with open(self.file_path, "r") as file:
            return json.loads(file.read())
