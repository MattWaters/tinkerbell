from abc import ABC, abstractmethod
from typing import Dict


# This class is a subclass of ABC, which stands for "Abstract Base Class".  Abstract classes are classes that can be
# used as parent classes, but CANNOT be instantiated directly.  foo = Storage() will fail.
class Storage(ABC):
    # Abstract methods are methods that force child classes to implement them, but cannot be called directly
    @abstractmethod
    def fetch_praw_config(self) -> Dict[str, str]:
        pass
