import json
from typing import Dict

import boto3

from tinkerbell.storage import Storage


class S3Client(Storage):
    def __init__(self, bucket_name: str, object_key: str, aws_access_key: str=None, aws_secret_key: str=None) -> None:
        # This will automatically fall back to an IAM role if aws_access_key and aws_secret_access_key are not supplied.
        self.s3 = boto3.Session(
            aws_access_key_id=aws_access_key,
            aws_secret_access_key=aws_secret_key
        ).client("s3")

        self.bucket_name = bucket_name
        self.object_key = object_key

    def fetch_praw_config(self) -> Dict[str, str]:
        praw_config = json.loads(
            self.s3.get_object(
                Bucket=self.bucket_name,
                Key=self.object_key
            )["Body"].read().decode("utf-8")
        )

        return praw_config
